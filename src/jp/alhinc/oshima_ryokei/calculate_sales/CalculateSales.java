package jp.alhinc.oshima_ryokei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;



public class CalculateSales {
	public static void main (String[] args){
		if(args.length!=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		HashMap<String, String> branchList = new HashMap<String, String>();
		HashMap<String, Long> salesResultMap = new HashMap <String, Long>();
		String instFileName = "branch.lst";
		String outPutFileName = "branch.out";
		String fileKindName = "支店";
		String collectCode = ("^[0-9]{3}$");
		
		if(!(fileRead(args[0],instFileName,collectCode,fileKindName,branchList,salesResultMap))){
			return;
		
		}
		
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File file, String fileName) {
				File rcdFile = new File(file,fileName);
				if (fileName.matches("^[0-9]{8}.rcd$")&&(rcdFile.isFile())){
					return true;
				} else {
					return false;
				}
			}
		
		};
		
		File[] fileList = new File(args[0]).listFiles(filter);
		for(int i = 0; i< fileList.length-1; i++){
			String fileName = fileList[i].getName();
			String fileName2 = fileList[i+1].getName();
			int fileNum = Integer.parseInt(fileName.substring(0,8));
			int fileNum2 = Integer.parseInt(fileName2.substring(0,8));
			if (fileNum2 - fileNum != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
				
			}
		}
		

		
		File[] files = new File(args[0]).listFiles(filter);
		
		BufferedReader br1 = null;

		for (int i = 0; i<files.length;i++){
			List<String> salesResultList = new ArrayList<String>();
			try {
				File salesResult = files[i];
				FileReader srfr = new FileReader(salesResult);
				br1 = new BufferedReader(srfr);

				String srbrline;
				while((srbrline = br1.readLine()) != null){
					salesResultList.add(srbrline);
				}
				if(salesResultList.size()!=2){
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
				if(salesResultList.get(1).matches("^[0-9]*$")!=true){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//branchListにないコードのものではなく、そもそも「マップにないコード」と書ける
				if(!(salesResultMap.containsKey(salesResultList.get(0)))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}
				
				long sumSold = 0;
				sumSold = salesResultMap.get(salesResultList.get(0)) + Long.parseLong(salesResultList.get(1));
				salesResultMap.put(salesResultList.get(0),sumSold);
				
				if (sumSold > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally{
				try {
					if( br1 != null){
						br1.close();
					}
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		
		}
		if(!(fileWrite(args[0],outPutFileName,branchList,salesResultMap))){
			return;
		}

	}
	
	
	public static boolean fileRead(String dirPath, String instFile,String kindCode, String kindName, HashMap<String, String> branchNameMap,HashMap <String, Long>sumSoldNum){
		
		BufferedReader br = null;
		try {
			File file = new File(dirPath, instFile);
			if (file.exists()!=true){
				System.out.println(kindName + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				String[] branch = line.split("\\,");
				
				if(branch.length!=2){
					System.out.println(kindName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				if(branch[0].matches(kindCode)!=true){
					System.out.println(kindName +  "定義ファイルのフォーマットが不正です");
					return false;
				}
				branchNameMap.put(branch[0], branch[1]);
				sumSoldNum.put(branch[0], (long) 0);
				
			}
			
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null){
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		
		return true;
		
	}
	
	
	public static boolean fileWrite(String dirPath, String outPutFile, HashMap<String, String> branchNameMap,HashMap<String, Long>sumSoldNum){
		
		BufferedWriter bw = null;
		try{
			File file = new File (dirPath, outPutFile);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter (fw);
			for (Entry<String,String > fileOutPut : branchNameMap.entrySet()){
				bw.write(fileOutPut.getKey()+","+fileOutPut.getValue()+","+sumSoldNum.get(fileOutPut.getKey()));
				bw.newLine();
			}
		
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			try {
				if (bw != null){
					bw.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
	
}
		
		
	



	
		






